<?php

namespace App\Http\Controllers;

use App\Models\Products_table;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProductsTableController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('products', [
            'products' => Products_table::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('addProducts');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'description' => 'required',
            'price' => 'required',
        ]);
        $path = Storage::putFile('images', $request->file('image'));



        Products_table::create([
            'name' => $request->name,
            'description' => $request->description,
            'price' => $request->price,
            'image' => $path
        ]);

        return redirect()->route('products')
                        ->with('success','Product created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Products_table  $products_table
     * @return \Illuminate\Http\Response
     */
    public function show(Products_table $products_table)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Products_table  $products_table
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('products/edit', [
            'product' => Products_table::find($id)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Products_table  $products_table
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $products = Products_table::find($request->id);

        $request->validate([
            'name' => 'required',
            'price' => 'required',
            'description' => 'required',
        ]);

        $file = $request->file('image');

        $path = $products->image;

        if(file_exists($file)){
            Storage::delete($products->image);
            $path = Storage::putFile('images', $file);
        }

        $products->update([
            'name' => $request->name,
            'description' => $request->description,
            'price' => $request->price,
            'image' => $path,
        ]);
        return redirect()->route('products')
                        ->with('success','Product created successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Products_table  $products_table
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $product = Products_table::find($request->id);
        Storage::delete($product->image);
        $product->delete();
        return redirect()->back();
    }
}
